// créer la formApp appLevelUp
export class suiviPersoApp extends FormApplication {



    constructor(actor, options = {}) {
        super(options);
        this.actor = actor;

    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ['form', 'HDsuivi'],
            popOut: true,
            closeOnSubmit: false,
            template: `modules/H&D suivi de perso/templates/appSuivi.html`,
            id: 'suiviPerso',
            title: 'suivi de personnage',
            width: 600,
            height: 300
        });
    }

    async getData() {





        // Return data to the template

        return {
            actor: this.actor,
        };
    }

    activateListeners(html) {
        super.activateListeners(html);
    }


}