export const HDSRD = {};
//-------------récup les races
HDSRD.races = [];
async function getRaces(index) {
    for (let r of index) {
        HDSRD.races.push(r.name)
    };
};
//-------------récup les historiques
HDSRD.historiques = [];
async function getHist(index) {
    for (let h of index) {
        HDSRD.historiques.push(h.name)
    };
};
HDSRD.traitsRaciaux = [];
async function gettraitsRaciaux(index) {
    for (let tr of index) {
        HDSRD.traitsRaciaux.push(tr.name);
    }

};

Hooks.once("ready", async function() {

    game.packs.get("srd-heros-et-dragons.h-d-races").getIndex().then(index =>
        getRaces(index)
    );
    game.packs.get("srd-heros-et-dragons.h-d-traits-raciaux").getIndex().then(index =>
        gettraitsRaciaux(index)
    );
    game.packs.get("srd-heros-et-dragons.h-d-historiques").getIndex().then(index =>
        getHist(index)
    );


})