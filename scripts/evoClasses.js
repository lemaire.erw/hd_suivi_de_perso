export const evoClasses = {
    barbare: {
        niveau: {
            1: ["Rage", "Défense sans armure"],
            2: ["Témérité", "Sens du danger"],
            3: ["Voie primitive"],
            4: ["Amélioration de caractéristiques"],
            5: ["Attaque supplémentaire", "Déplacement rapide"],
            6: ["Aptitude de voie"],
            7: ["Instinct sauvage"],
            8: ["Amélioration de caractéristiques"],
            9: ["Critique brutal (1 dé)"],
            10: ["Aptitude de voie"],
            11: ["Rage implacable"],
            12: ["Amélioration de caractéristiques"],
            13: ["Critique brutal (2 dés)"],
            14: ["Aptitude de voie"],
            15: ["Rage ininterrompue"],
            16: ["Amélioration de caractéristiques"],
            17: ["Critique brutal (3 dés)"],
            18: ["Puissance indomptable"],
            19: ["Amélioration de caractéristiques"],
            20: ["Champion primitif"]
        },
        spécialisation: {

        }
    }

}