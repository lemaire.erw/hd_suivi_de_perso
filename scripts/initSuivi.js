import { evoClasses } from './evoClasses.js';

import { suiviPersoApp } from './appSuivi.js';
import { creaPersoApp } from './appCrea.js';


Hooks.once('init', async function() {


    console.log("-------------initi suivi de personnage");


});


Hooks.once('ready', async function() {




    function setHDflags() {

    };
    setHDflags();

});
Hooks.on("renderActorSheet", (app, html, data) => {

    //-----crée l'app de suivi en fonction des data de la fiche de perso: 

    let suivi = new suiviPersoApp(data.actor);
    async function openSuivi() {
        suivi.render(true);
    }
    let crea = new creaPersoApp(data)
    async function openCrea() {
        console.log(data);
        crea.render(true);
    }

    if (data.isCharacter && data.owner) {

        let hder;
        //------defini feuille de perso dnd5/tidy??

        let sheetTidy = data.options.classes.find(cl => cl == "tidy5e");

        if (sheetTidy) {
            hder = html.find('header.flex.tidy5e-header');
        }
        if (!sheetTidy) {
            hder = html.find('header.sheet-header');
        };

        //----ajout du bouton pour ouvrir le suivi


        hder.prepend('<div class="fexrow HDsuivi"style="max-width:45px"><div id="HDsuivi" style="max-width : 30px" >suivi de perso</div><div id="HDcrea" style="max-width : 30px" >créa de perso</p></div>');
        var btncrea = document.getElementById("HDcrea");
        var btnUp = document.getElementById("HDsuivi");
        btnUp.addEventListener("click", () => { openSuivi(); }, false);
        btncrea.addEventListener("click", () => { openCrea(); }, false);

    }





});