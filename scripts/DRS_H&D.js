export const SRDHD = {
    /*---------------RACES-----------------
    --------------------------------------*/
    races: {

    },





    hitoriques: {
        /* ----------- HISTORIQUES----------------
    ------------------------------------------
   
      NOM HIS: {
            nom:""
            compétences: [],
            outils: [],
            langues: [],
            équipement: [],
            aptitude: [],

            Trait: [
                
            ],
            Idéal: [
                
            ],
            Lien: [
                
            ],
            Défaut:[
            
            ]
        },

        */



        brigands: {
            nom: "Brigand",
            compétences: ["Discrétion", "Survie"],
            outils: ["Véhicules (terrestres)", "un type de jeu au choix"],
            langues: [],
            équipement: ["Couverture", "vêtements de voyageur", "piège à mâchoires", "boîte à amadou", "matériel de pêche", "outre d'eau", "bourse contenant 10 po"],
            aptitude: ["signe de piste (Brigand)", "Milieu naturel de prédilection"],

            Trait: [
                "Je suis extrêmement méfiant. Ma confiance a un prix, et ce prix est élevé.",
                "Je tourne toute situation en dérision, et fais fi des sentiments des autres.",
                "Je ne suis à mon aise qu'avec le ciel étoilé comme toit.",
                "Je ne recule jamais face à un défi, aussi insurmontable soit-il.",
                "Je ne me soucie guère des problèmes des autres.",
                "Ma parole est mon bien le plus précieux, je ne la donne jamais à la légère.",
                "Je protège les miens comme une mère protège ses petits.",
                "Je suis aussi impitoyable que la nature peut l'être. La survie est à ce prix."
            ],
            Idéal: [
                "Honneur.La société peut penser ce qu 'elle veut de vous. Ce qui vous importe avant tout, c'est votre conscience.(Loyal)",
                "Pouvoir.Vous faites ce qui vous chante et prenez ce dont vous avez besoin quand vous en avez besoin.(Mauvais)",
                "Nature.Vous avez développé un lien particulier avec la nature et souhaitez la préserver de la corruption de la civilisation.(Neutre)",
                "Bien commun.Vous avez développé un sens élevé du partage,et vous mettez souvent l 'intérêt de la communauté avant le vôtre. (Bon)",
                "Liberté.La vie en société est faite de chaînes et de restrictions et vous ne comptez pas vous laisser emprisonner.(Chaotique)",
                "Autonomie.Chacun doit pouvoir être en mesure d 'assurer sa propre survie. (Neutre)"
            ],
            Lien: [
                "Certaines personnes dépendent de moi(famille, bande, amis) et je veux être digne de leur attachement.",
                "Ma région d 'origine a une grande importance à mes yeux, et je ferai tout ce qui est en mon pouvoir pour la préserver.",
                "J'ai fait une promesse à l'un de mes proches disparus,et je tiendrai parole.",
                "L 'un des miens m'a été enlevé.Je suis persuadé qu 'il est encore en vie quelque part, et j'ai juré de le retrouver et de le ramener chez lui.",
                "Ma région a été frappée par un désastre,et c 'est à moi qu'il revient de trouver le moyen de restaurer ce qui peut l 'être.",
                "J 'ai déniché une information capitale (secret, carte au trésor, etc.) qui pourrait assurer définitivement la prospérité de ma communauté, et je compte bien en profiter.",
            ],
            Défaut: [
                "Je ne pardonne jamais une offense ou une trahison.",
                "Par ignorance ou par mépris des conventions,j'apparais souvent comme un rustre.",
                "J 'ai tendance à penser que ma solution est toujours la meilleure.",
                "Je parle souvent avant de réfléchir.",
                "Je n 'accorde aucun crédit aux citadins et aux nantis.",
                "J 'ai une confiance excessive en mes propres capacités."
            ]
        },
        Bandit_de_grand_chemin: {
            nom: "Bandit de grand chemin",
            compétences: ["Intimidation", "Survie"],
            outils: ["Véhicules (terrestres)"],
            langues: ["Une langue au choix"],
            équipement: ["Couverture", "vêtements de voyageur", "quelques chausse-trappes", "boîte à amadou", "outre d'eau, bourse contenant 10 po"],
            aptitude: ["signe de piste (Brigand)", "Milieu naturel de prédilection"],

            Trait: [
                "Je suis extrêmement méfiant. Ma confiance a un prix, et ce prix est élevé.",
                "Je tourne toute situation en dérision, et fais fi des sentiments des autres.",
                "Je ne suis à mon aise qu'avec le ciel étoilé comme toit.",
                "Je ne recule jamais face à un défi, aussi insurmontable soit-il.",
                "Je ne me soucie guère des problèmes des autres.",
                "Ma parole est mon bien le plus précieux, je ne la donne jamais à la légère.",
                "Je protège les miens comme une mère protège ses petits.",
                "Je suis aussi impitoyable que la nature peut l'être. La survie est à ce prix."
            ],
            Idéal: [
                "Honneur.La société peut penser ce qu 'elle veut de vous. Ce qui vous importe avant tout, c'est votre conscience.(Loyal)",
                "Pouvoir.Vous faites ce qui vous chante et prenez ce dont vous avez besoin quand vous en avez besoin.(Mauvais)",
                "Nature.Vous avez développé un lien particulier avec la nature et souhaitez la préserver de la corruption de la civilisation.(Neutre)",
                "Bien commun.Vous avez développé un sens élevé du partage,et vous mettez souvent l 'intérêt de la communauté avant le vôtre. (Bon)",
                "Liberté.La vie en société est faite de chaînes et de restrictions et vous ne comptez pas vous laisser emprisonner.(Chaotique)",
                "Autonomie.Chacun doit pouvoir être en mesure d 'assurer sa propre survie. (Neutre)"
            ],
            Lien: [
                "Certaines personnes dépendent de moi(famille, bande, amis) et je veux être digne de leur attachement.",
                "Ma région d 'origine a une grande importance à mes yeux, et je ferai tout ce qui est en mon pouvoir pour la préserver.",
                "J'ai fait une promesse à l'un de mes proches disparus,et je tiendrai parole.",
                "L 'un des miens m'a été enlevé.Je suis persuadé qu 'il est encore en vie quelque part, et j'ai juré de le retrouver et de le ramener chez lui.",
                "Ma région a été frappée par un désastre,et c 'est à moi qu'il revient de trouver le moyen de restaurer ce qui peut l 'être.",
                "J 'ai déniché une information capitale (secret, carte au trésor, etc.) qui pourrait assurer définitivement la prospérité de ma communauté, et je compte bien en profiter.",
            ],
            Défaut: [
                "Je ne pardonne jamais une offense ou une trahison.",
                "Par ignorance ou par mépris des conventions,j'apparais souvent comme un rustre.",
                "J 'ai tendance à penser que ma solution est toujours la meilleure.",
                "Je parle souvent avant de réfléchir.",
                "Je n 'accorde aucun crédit aux citadins et aux nantis.",
                "J 'ai une confiance excessive en mes propres capacités."
            ]
        },

        Fugitif: {
            nom: "Fugitif ",
            compétences: ["Survie", "Supercherie"],
            outils: ["Un type de jeu au choix", "un ensemble d'outils d'artisan au choix", "vestiges de sa vie passée"],
            langues: ["Une langue au choix"],
            équipement: ["Sac avec tente et couverture", "vêtements de voyageur", "outils d'artisan", "boîte à amadou", "outre d'eau", "bourse contenant 10 po"],
            aptitude: ["signe de piste (Brigand)", "Milieu naturel de prédilection"],

            Trait: [
                "Je suis extrêmement méfiant. Ma confiance a un prix, et ce prix est élevé.",
                "Je tourne toute situation en dérision, et fais fi des sentiments des autres.",
                "Je ne suis à mon aise qu'avec le ciel étoilé comme toit.",
                "Je ne recule jamais face à un défi, aussi insurmontable soit-il.",
                "Je ne me soucie guère des problèmes des autres.",
                "Ma parole est mon bien le plus précieux, je ne la donne jamais à la légère.",
                "Je protège les miens comme une mère protège ses petits.",
                "Je suis aussi impitoyable que la nature peut l'être. La survie est à ce prix."
            ],
            Idéal: [
                "Honneur.La société peut penser ce qu 'elle veut de vous. Ce qui vous importe avant tout, c'est votre conscience.(Loyal)",
                "Pouvoir.Vous faites ce qui vous chante et prenez ce dont vous avez besoin quand vous en avez besoin.(Mauvais)",
                "Nature.Vous avez développé un lien particulier avec la nature et souhaitez la préserver de la corruption de la civilisation.(Neutre)",
                "Bien commun.Vous avez développé un sens élevé du partage,et vous mettez souvent l 'intérêt de la communauté avant le vôtre. (Bon)",
                "Liberté.La vie en société est faite de chaînes et de restrictions et vous ne comptez pas vous laisser emprisonner.(Chaotique)",
                "Autonomie.Chacun doit pouvoir être en mesure d 'assurer sa propre survie. (Neutre)"
            ],
            Lien: [
                "Certaines personnes dépendent de moi(famille, bande, amis) et je veux être digne de leur attachement.",
                "Ma région d 'origine a une grande importance à mes yeux, et je ferai tout ce qui est en mon pouvoir pour la préserver.",
                "J'ai fait une promesse à l'un de mes proches disparus,et je tiendrai parole.",
                "L 'un des miens m'a été enlevé.Je suis persuadé qu 'il est encore en vie quelque part, et j'ai juré de le retrouver et de le ramener chez lui.",
                "Ma région a été frappée par un désastre,et c 'est à moi qu'il revient de trouver le moyen de restaurer ce qui peut l 'être.",
                "J 'ai déniché une information capitale (secret, carte au trésor, etc.) qui pourrait assurer définitivement la prospérité de ma communauté, et je compte bien en profiter.",
            ],
            Défaut: [
                "Je ne pardonne jamais une offense ou une trahison.",
                "Par ignorance ou par mépris des conventions,j'apparais souvent comme un rustre.",
                "J 'ai tendance à penser que ma solution est toujours la meilleure.",
                "Je parle souvent avant de réfléchir.",
                "Je n 'accorde aucun crédit aux citadins et aux nantis.",
                "J 'ai une confiance excessive en mes propres capacités."
            ]
        },

        Pirate: {
            nom: "Pirate",
            compétences: ["Athlétisme", "Supercherie"],
            outils: ["Instruments de navigation", "véhicules (marins)"],
            langues: ["Une langue au choix"],
            équipement: ["Couverture", "vêtements de voyageur", "longue-vue", "instruments de navigation", "matériel de pêche", "bouteille d'alcool fort", "bourse contenant 10 po"],
            aptitude: ["Confrérie"],


            Trait: [
                "Je suis extrêmement méfiant. Ma confiance a un prix, et ce prix est élevé.",
                "Je tourne toute situation en dérision, et fais fi des sentiments des autres.",
                "Je ne suis à mon aise qu'avec le ciel étoilé comme toit.",
                "Je ne recule jamais face à un défi, aussi insurmontable soit-il.",
                "Je ne me soucie guère des problèmes des autres.",
                "Ma parole est mon bien le plus précieux, je ne la donne jamais à la légère.",
                "Je protège les miens comme une mère protège ses petits.",
                "Je suis aussi impitoyable que la nature peut l'être. La survie est à ce prix."
            ],
            Idéal: [
                "Honneur.La société peut penser ce qu 'elle veut de vous. Ce qui vous importe avant tout, c'est votre conscience.(Loyal)",
                "Pouvoir.Vous faites ce qui vous chante et prenez ce dont vous avez besoin quand vous en avez besoin.(Mauvais)",
                "Nature.Vous avez développé un lien particulier avec la nature et souhaitez la préserver de la corruption de la civilisation.(Neutre)",
                "Bien commun.Vous avez développé un sens élevé du partage,et vous mettez souvent l 'intérêt de la communauté avant le vôtre. (Bon)",
                "Liberté.La vie en société est faite de chaînes et de restrictions et vous ne comptez pas vous laisser emprisonner.(Chaotique)",
                "Autonomie.Chacun doit pouvoir être en mesure d 'assurer sa propre survie. (Neutre)"
            ],
            Lien: [
                "Certaines personnes dépendent de moi(famille, bande, amis) et je veux être digne de leur attachement.",
                "Ma région d 'origine a une grande importance à mes yeux, et je ferai tout ce qui est en mon pouvoir pour la préserver.",
                "J'ai fait une promesse à l'un de mes proches disparus,et je tiendrai parole.",
                "L 'un des miens m'a été enlevé.Je suis persuadé qu 'il est encore en vie quelque part, et j'ai juré de le retrouver et de le ramener chez lui.",
                "Ma région a été frappée par un désastre,et c 'est à moi qu'il revient de trouver le moyen de restaurer ce qui peut l 'être.",
                "J 'ai déniché une information capitale (secret, carte au trésor, etc.) qui pourrait assurer définitivement la prospérité de ma communauté, et je compte bien en profiter.",
            ],
            Défaut: [
                "Je ne pardonne jamais une offense ou une trahison.",
                "Par ignorance ou par mépris des conventions,j'apparais souvent comme un rustre.",
                "J 'ai tendance à penser que ma solution est toujours la meilleure.",
                "Je parle souvent avant de réfléchir.",
                "Je n 'accorde aucun crédit aux citadins et aux nantis.",
                "J 'ai une confiance excessive en mes propres capacités."
            ]
        },


        /*-----fin historiques---------------------------*/
    }



}